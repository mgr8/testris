import sys
import mino as Mino


class Matrix():
    def __init__(self):
        self.score = 0
        self.lines_cleared = 0
        self.matrix = [[]] * 22
        self.clear_matrix()
        self.active = None
        self.matrix_with_active = None
        self.active_mino_start = None
        self.active_mino_top = None

    def print_matrix(self, matrix, upper=False):
        for x in matrix:
            for y in x:
                print(y.upper() if upper else y, end=' ')
            print()

    def read_lines_for_matrix(self):
        lines = []
        for _ in range(22):
            lines.append(input().strip())

        return lines

    def read_matrix_from_lines(self, lines):
        for index, line in enumerate(lines):
            self.matrix[index] = [x for x in line.strip().split()]

    def clear_matrix(self):
        for x in range(22):
            self.matrix[x] = ['.'] * 10

    def read_matrix(self):
        self.read_matrix_from_lines(self.read_lines_for_matrix())

    def check_lines_to_clear(self):
        for index, line in enumerate(self.matrix):
            if '.' not in line:
                self.score += 100
                self.lines_cleared += 1
                self.matrix[index] = ['.'] * 10

    def print_score(self):
        print(self.score)

    def print_lines_cleared(self):
        print(self.lines_cleared)

    def set_active(self, shape):
        self.active = Mino.Mino(shape)
        self.active_mino_top = 0
        self.matrix_with_active, self.active_mino_start = self.spawn_mino_in_matrix(
            self.matrix, self.active)

    def print_active(self):
        if self.active == None:
            return
        self.active.print_mino()

    def rotate_active_clockwise(self):
        if self.active == None:
            return
        self.active.rotate_clockwise()

    def rotate_active_anti_clockwise(self):
        if self.active == None:
            return
        self.active.rotate_anti_clockwise()

    def nudge_active_left(self):
        self.active_mino_start -= 1
        _size, _start, _top = len(
            self.active.shape_matrix), self.active_mino_start, self.active_mino_top
        self.matrix_with_active[_top][_start: _start +
                                      _size + 1] = self.active.shape_matrix[0] + ['.']
        self.matrix_with_active[_top + 1][_start: _start +
                                          _size + 1] = self.active.shape_matrix[1] + ['.']

    def nudge_active_right(self):
        self.active_mino_start += 1
        _size, _start, _top = len(
            self.active.shape_matrix), self.active_mino_start, self.active_mino_top
        self.matrix_with_active[_top][_start - 1: _start +
                                      _size] = ['.'] + self.active.shape_matrix[0]
        self.matrix_with_active[_top + 1][_start - 1: _start +
                                          _size] = ['.'] + self.active.shape_matrix[1]

    def nudge_active_down(self):
        _size, _start, _top = len(
            self.active.shape_matrix), self.active_mino_start, self.active_mino_top
        self.matrix_with_active[_top][_start: _start +
                                      _size] = ['.'] * (_size)
        _top += 1
        for x in range(_size):
            self.matrix_with_active[_top + x][_start: _start +
                                              _size] = self.active.shape_matrix[x]
        self.active_mino_top = _top

    def spawn_mino_in_matrix(self, matrix, Mino):
        mino_size, mino_shape = len(Mino.shape_matrix), Mino.shape
        if mino_shape == 'O':
            mino_start = 4
        elif mino_shape in ['L', 'J', 'Z', 'S', 'I', 'T']:
            mino_start = 3

        matrix[0][mino_start: mino_start +
                  mino_size] = Mino.shape_matrix[0]
        matrix[1][mino_start: mino_start +
                  mino_size] = Mino.shape_matrix[1]
        return matrix, mino_start

    def print_game_matrix(self):
        self.print_matrix(self.matrix)

    def print_matrix_with_active(self):
        self.print_matrix(self.matrix_with_active, upper=True)

    def exit(self):
        sys.exit(-1)
