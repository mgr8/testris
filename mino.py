class Mino():
    def __init__(self, shape):
        self.shape = shape
        self.give_shape(shape)

    def give_shape(self, shape):
        if shape == 'I':
            self.shape_matrix = self.gen_matrix(4)
            self.shape_matrix[1] = ['c'] * 4
        elif shape == 'O':
            self.shape_matrix = self.gen_matrix(2)
            for index, _ in enumerate(self.shape_matrix):
                self.shape_matrix[index] = ['y'] * 2
        elif shape == 'Z':
            self.shape_matrix = self.gen_matrix(3)
            self.shape_matrix[0] = ['r', 'r', '.']
            self.shape_matrix[1] = ['.', 'r', 'r']
        elif shape == 'S':
            self.shape_matrix = self.gen_matrix(3)
            self.shape_matrix[0] = ['.', 'g', 'g']
            self.shape_matrix[1] = ['g', 'g', '.']
        elif shape == 'J':
            self.shape_matrix = self.gen_matrix(3)
            self.shape_matrix[0][0] = 'b'
            self.shape_matrix[1] = ['b'] * 3
        elif shape == 'L':
            self.shape_matrix = self.gen_matrix(3)
            self.shape_matrix[0][2] = 'o'
            self.shape_matrix[1] = ['o'] * 3
        elif shape == 'T':
            self.shape_matrix = self.gen_matrix(3)
            self.shape_matrix[0][1] = 'm'
            self.shape_matrix[1] = ['m'] * 3

    def print_mino(self):
        for y in self.shape_matrix:
            for x in y:
                print(x, end=' ')

            print()

    def rotate_clockwise(self):
        self.shape_matrix = list(zip(*self.shape_matrix[::-1]))

    def rotate_anti_clockwise(self):
        self.shape_matrix = list(zip(*self.shape_matrix))[::-1]

    def gen_matrix(self, size):
        matrix = [[]] * size
        for index, _ in enumerate(matrix):
            matrix[index] = ['.'] * size
        return matrix
